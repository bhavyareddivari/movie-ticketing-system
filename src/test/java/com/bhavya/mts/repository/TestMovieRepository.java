package com.bhavya.mts.repository;

import java.util.Date;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.impl.MovieImpl;
import com.bhavya.mts.repository.MovieRepository;

@ContextConfiguration(locations={"classpath:spring-context.xml"})
public class TestMovieRepository extends AbstractTransactionalJUnit4SpringContextTests{
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MovieRepository movieRepository;
	
	@Test
	//@Rollback(false)
	public void addAndGetMovie(){
		MovieImpl newMovie=new MovieImpl();
		newMovie.setTitle("Steve Jobs");
		newMovie.setDescription("About history of SteveJobs-Founder of Apple");
		newMovie.setDuration(2.30);
		newMovie.setReleaseDate(new Date());
	
		int addedMovieId=movieRepository.addMovie(newMovie);
		logger.info("user added "+addedMovieId);
		Assert.assertNotEquals(0, addedMovieId);
		
		Movie movie=movieRepository.getMovie(addedMovieId);
		Assert.assertEquals(movie.getId(),addedMovieId);
		
	}
}
