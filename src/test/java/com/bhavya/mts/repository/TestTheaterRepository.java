package com.bhavya.mts.repository;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.bhavya.mts.entity.impl.TheaterImpl;
import com.bhavya.mts.repository.TheaterRepository;
import com.bhavya.mts.entity.Theater;

@ContextConfiguration(locations={"classpath:spring-context.xml"})
public class TestTheaterRepository extends AbstractTransactionalJUnit4SpringContextTests{

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private TheaterRepository theaterRepository;
	
	@Test
	//@Rollback(false)
	public void addAndGetTheater(){
		TheaterImpl newTheater=new TheaterImpl();
		newTheater.setLocation("eastridge");
		newTheater.setName("amc");
		newTheater.setZipcode(95122);
	
		int addedTheaterId=theaterRepository.addTheater(newTheater);
		logger.info("Theater added "+addedTheaterId);
		Assert.assertNotEquals(0, addedTheaterId);
		
		Theater theater=theaterRepository.getTheater(addedTheaterId);
		Assert.assertEquals(theater.getId(),addedTheaterId);
		
	}
}
