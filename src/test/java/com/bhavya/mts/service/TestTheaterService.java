package com.bhavya.mts.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bhavya.mts.repository.TheaterRepository;
import com.bhavya.mts.entity.Theater;

@ContextConfiguration(locations= {"classpath:spring-context.xml"})
public class TestTheaterService extends AbstractJUnit4SpringContextTests {

	@Autowired
	public TheaterRepository theaterRepository;
	
	@Test
	public void getTheatersTest(){
		Theater theater=theaterRepository.getTheater(4);
		Assert.assertNotNull(theater);
		Assert.assertEquals(theater.getName(), "amc");
		Assert.assertNotEquals(theater.getLocation(), "london");
	}	
	
}

