package com.bhavya.mts.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bhavya.mts.repository.MovieRepository;
import com.bhavya.mts.entity.Movie;

@ContextConfiguration(locations= {"classpath:spring-context.xml"})
public class TestMovieService extends AbstractJUnit4SpringContextTests {

	@Autowired
	public MovieRepository movieRepository;
		
	@Test
	public void getMoviesTest(){
		Movie movie=movieRepository.getMovie(6);
		Assert.assertNotNull(movie);
		Assert.assertEquals(movie.getTitle(),"Steve Jobs");
		Assert.assertNotEquals(movie.getDuration(), "7888");
	}
}
