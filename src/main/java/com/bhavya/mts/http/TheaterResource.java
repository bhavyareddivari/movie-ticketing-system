package com.bhavya.mts.http;

import java.util.ArrayList;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bhavya.mts.entity.Theater;
import com.bhavya.mts.entity.impl.TheaterImpl;
import com.bhavya.mts.http.entity.HttpTheater;
import com.bhavya.mts.service.TheaterService;
import com.bhavya.mts.service.exception.MTSException;

@Path("/theater")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class TheaterResource {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private TheaterService theaterService;
	
	@POST
	@Path("/")
	public Response createTheater(HttpTheater newTheater) {
		Theater theaterToCreate = convert(newTheater);
		Theater addedTheater = theaterService.addTheater(theaterToCreate);
		return Response.status(Status.CREATED).header("Location", "/theater/" + addedTheater.getId()).entity(new HttpTheater(addedTheater)).build();
	}

	@GET
	@Path("/{theaterId}")
	public HttpTheater getTheaterById(@PathParam("theaterId") int theaterId) {
		logger.info("getting user by id:" + theaterId);
		Theater theater = theaterService.getTheaters(theaterId);
		HttpTheater httpTheater = new HttpTheater(theater);
		return httpTheater;
	}
	
	@GET
	@Path("/")
	@Wrapped(element = "theater")
	public List<HttpTheater> getTheaterSearch(@QueryParam("location") String location) throws MTSException {
		List<Theater> found = (List<Theater>) theaterService.getTheaterByLocation(location);
		List<HttpTheater> returnList = new ArrayList<>(found.size());
		for (Theater theater : found) {
			returnList.add(new HttpTheater(theater));
		}
		return returnList;
	}

	private Theater convert(HttpTheater newTheater) {
		TheaterImpl theater = new TheaterImpl();
		theater.setName(newTheater.name);
		theater.setLocation(newTheater.location);
		theater.setZipcode(newTheater.zipcode);
		return theater;
	}
}
