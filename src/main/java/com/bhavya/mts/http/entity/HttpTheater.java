package com.bhavya.mts.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bhavya.mts.entity.Theater;

@XmlRootElement(name = "theater")
public class HttpTheater {

	@XmlElement
	public int id;

	@XmlElement
	public String name;

	@XmlElement
	public String location;

	@XmlElement
	public int zipcode;

	public HttpTheater() {
	}

	public HttpTheater(Theater theater) {
		this.id = theater.getId();
		this.name = theater.getName();
		this.location = theater.getLocation();
		this.zipcode = theater.getZipcode();
	}

}
