package com.bhavya.mts.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.bhavya.mts.entity.Movie;

@XmlRootElement(name = "movie")
public class HttpMovie {
	
	@XmlElement
	public int id;
	
	@XmlElement
	public String title;
	
	@XmlElement
	public String description;

	@XmlElement
	public double duration;
	
	@XmlElement
	public Date releaseDate;
	
	//required by framework
	public HttpMovie() {}
	
	

	public HttpMovie(Movie movie) {
		this.id=movie.getId();
		this.title=movie.getTitle();
		this.description=movie.getDescription();
		this.duration=movie.getDuration();
		this.releaseDate=movie.getReleaseDate();
	}
}
