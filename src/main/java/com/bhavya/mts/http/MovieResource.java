package com.bhavya.mts.http;

import java.util.ArrayList;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.impl.MovieImpl;
import com.bhavya.mts.http.entity.HttpMovie;
import com.bhavya.mts.service.MovieService;
import com.bhavya.mts.service.exception.MTSException;


@Path("/movie")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class MovieResource {
	
	@Autowired
	private MovieService movieService;
	
	@POST
	@Path("/postMovie/")
	public Response postMovie(@QueryParam("title") String title,@QueryParam("description") String description,@QueryParam("duration") double duration){		
		MovieImpl  addedMovie=movieService.addMovie(title,description,duration);
		Response response=Response.status(Status.CREATED).header("Location", "/movie/"+addedMovie.getId()).entity(new HttpMovie(addedMovie)).build();		
		return response;
	}
	@GET
	@Path("/{movieId}")	
	public HttpMovie getMovieById(@PathParam("movieId") int movieId){
		Movie movie = movieService.getMovies(movieId);
		HttpMovie httpMovie= new HttpMovie(movie);
		return httpMovie;
	}

	@GET
	@Path("/")
	@Wrapped(element="movie")
	public List<HttpMovie> getMovieSearch(@QueryParam("title") String title) throws MTSException{
		List<Movie> found = (List<Movie>) movieService.getMovieNames(title);
		List<HttpMovie> returnList = new ArrayList<>(found.size());
		for(Movie movie:found){
			returnList.add(new HttpMovie(movie));
		}
		return returnList;
	}
	
	@PUT
	@Path("/putMovie/")
	public void updateMovie(@QueryParam("movieId") int movieId,@QueryParam("description") String description,@QueryParam("title") String title){
		movieService.updateMovie(movieId,description,title);
	}
	
	@DELETE
	@Path("/delete/")
	public String deleteMovie(@QueryParam("movieId") int movieId){
		String rs=movieService.deleteMovie(movieId);
		return rs;
		
	}
	
}
