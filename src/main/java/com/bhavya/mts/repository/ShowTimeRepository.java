package com.bhavya.mts.repository;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.ShowTime;

public interface ShowTimeRepository {
	
	int addShowTime(ShowTime show);

	Movie getMovie(String start_time);
}
