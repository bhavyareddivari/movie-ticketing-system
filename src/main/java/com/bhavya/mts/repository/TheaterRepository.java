package com.bhavya.mts.repository;

import java.util.List;

import com.bhavya.mts.entity.Theater;

public interface TheaterRepository {

	int addTheater(Theater theater);
	 
	Theater getTheater(int id);
	
	Theater getTheaterByName(String name);
	
	List<Theater> getTheaterByLocation(String name);

}
