package com.bhavya.mts.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bhavya.mts.entity.Theater;
import com.bhavya.mts.entity.impl.TheaterImpl;
import com.bhavya.mts.repository.TheaterRepository;

@Repository
@Transactional
public class TheaterRepositoryImpl implements TheaterRepository {

	@Autowired
	private SessionFactory sessionFactory;

	public int addTheater(Theater theater) {
		return (Integer) this.sessionFactory.getCurrentSession().save(theater);
	}

	public com.bhavya.mts.entity.Theater getTheater(int id) {
		return (Theater) this.sessionFactory.getCurrentSession().get(TheaterImpl.class, id);
	}
	public Theater getTheaterByName(String name){
		return (Theater) this.sessionFactory.getCurrentSession().get(TheaterImpl.class, name);
	}
	
	public List<Theater> getTheaterByLocation(String location) {	
		List<Theater> theaterList = new ArrayList<>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TheaterImpl.class);
		criteria.add(Restrictions.ilike("location", location, MatchMode.ANYWHERE));
		theaterList = criteria.list();
		return theaterList;
	}
}
