package com.bhavya.mts.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.impl.MovieImpl;
import com.bhavya.mts.repository.MovieRepository;

@Repository
@Transactional
public class MovieRepositoryImpl implements MovieRepository {
	
	@Autowired
	private SessionFactory sessionFactory;

	public Movie getMovie(int id) {
		return (Movie) this.sessionFactory.getCurrentSession().get(MovieImpl.class, id);
	}
	
	public Movie getMovie(String movieName) {
		return (Movie)this.sessionFactory.getCurrentSession().get(MovieImpl.class, movieName);
	}
	public List<Movie> getMovieNames(String movieName) {	
		List<Movie> moviesList = new ArrayList<>();
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieImpl.class);
		criteria.add(Restrictions.ilike("title", movieName, MatchMode.ANYWHERE));
		moviesList = criteria.list();
		return moviesList;
	}
	
	public int addMovie(Movie movie) {
		return (Integer) this.sessionFactory.getCurrentSession().save(movie);
	}
	
	public void updateMovie(int movieId,String description, String title){
		
		MovieImpl movieImpl= new MovieImpl();
		movieImpl.setId(movieId);
		movieImpl.setDescription(description);
		movieImpl.setReleaseDate(new Date());
		movieImpl.setTitle(title);
		this.sessionFactory.getCurrentSession().update(movieImpl);
	}
	
	@Override
	public String deleteMovie(int movieId) {
		MovieImpl movieImpl= new MovieImpl();
		movieImpl.setId(movieId);
		this.sessionFactory.getCurrentSession().delete(movieImpl);
		return "Movie with id=" +movieId +" deleted";
	}

	@Override
	public MovieImpl addMovie(String title, String description, double duration) {
		MovieImpl movieImpl= new MovieImpl();
		movieImpl.setDescription(description);
		movieImpl.setReleaseDate(new Date());
		movieImpl.setTitle(title);
		movieImpl.setDuration(duration);
		this.sessionFactory.getCurrentSession().save(movieImpl);
		return movieImpl;
	}

}
