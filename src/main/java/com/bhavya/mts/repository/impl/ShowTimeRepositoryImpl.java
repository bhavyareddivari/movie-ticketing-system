package com.bhavya.mts.repository.impl;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.ShowTime;
import com.bhavya.mts.entity.impl.ShowTimeImpl;
import com.bhavya.mts.repository.ShowTimeRepository;

@Repository
@Transactional
public class ShowTimeRepositoryImpl implements ShowTimeRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Movie getMovie(String start_time) {
		return (Movie) this.sessionFactory.getCurrentSession().get(ShowTimeImpl.class, start_time);
	}

	public int addShowTime(ShowTime show) {
		return (Integer) this.sessionFactory.getCurrentSession().save(show);
	}

}
