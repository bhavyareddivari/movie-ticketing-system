package com.bhavya.mts.repository;

import java.util.List;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.impl.MovieImpl;

public interface MovieRepository {

	int addMovie(Movie movie);
	 
	Movie getMovie(int id);
	
	Movie getMovie(String name);
	
	void updateMovie(int id,String description, String tittle);
	
	List<Movie> getMovieNames(String movieName);
	
	String deleteMovie(int movieId);

	MovieImpl addMovie(String title, String description, double duration);


}
