package com.bhavya.mts.entity;

import java.util.Date;

public interface Movie {
	int getId();
	String getTitle();
	String getDescription();
	double getDuration();
	Date getReleaseDate();
}
