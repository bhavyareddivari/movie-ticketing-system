package com.bhavya.mts.entity;

public interface ShowTime {

	int getId();
	String getStartTime();
}
