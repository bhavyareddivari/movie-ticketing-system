package com.bhavya.mts.entity.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.ShowTime;
import com.bhavya.mts.entity.Theater;

@Entity
@Table(name="movie_showtime")
public class ShowTimeImpl implements ShowTime {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="start_time")
	private String start_time;
	
	@ManyToOne(targetEntity=MovieImpl.class)
	@JoinColumn(name="movies_id")	
	private Movie movie;
	
	@ManyToOne(targetEntity=TheaterImpl.class)
	@JoinColumn(name="theaters_id")	
	private Theater theater;
	
	public int getId() {
		return 0;
	}

	public String getStartTime() {
		return null;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	

	public Movie getMovie() {
		return movie;
	}

	public Theater getTheater() {
		return theater;
	}

}
