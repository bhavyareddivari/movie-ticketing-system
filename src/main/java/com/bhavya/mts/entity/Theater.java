package com.bhavya.mts.entity;

public interface Theater {
	int getId();
	String getName();
	String getLocation();
	int getZipcode();
}
