package com.bhavya.mts.service;

import java.util.List;

import com.bhavya.mts.entity.Theater;

public interface TheaterService {
	
	Theater addTheater(Theater theater);

	public Theater theatersByName(String theaterName);
			
	public Theater getTheaters(int id);

	List<Theater> getTheaterByLocation(String searchString);
			
}
