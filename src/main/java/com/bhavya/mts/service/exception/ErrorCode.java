package com.bhavya.mts.service.exception;

public enum ErrorCode {
	INVALID_FIELD,
	MISSING_DATA
}
