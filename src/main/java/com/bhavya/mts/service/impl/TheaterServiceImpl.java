package com.bhavya.mts.service.impl;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bhavya.mts.entity.Theater;
import com.bhavya.mts.entity.impl.TheaterImpl;
import com.bhavya.mts.repository.TheaterRepository;
import com.bhavya.mts.service.TheaterService;
import com.bhavya.mts.service.exception.ErrorCode;
import com.bhavya.mts.service.exception.InvalidFieldException;
import com.bhavya.mts.service.exception.MTSException;

@Service
public class TheaterServiceImpl implements TheaterService {
	private static final int MAX_NAME_LENGTH = 50;

	@Autowired
	private TheaterRepository theaterRepository;
	
	public Theater theatersByName(String theaterName) {
		return theaterRepository.getTheaterByName(theaterName);
	}

	public Theater getTheaters(int id) {	
		return theaterRepository.getTheater(id);
	}
	@Override
	@Transactional
	public List<Theater> getTheaterByLocation(String searchString) {
		List<Theater> theaterList =theaterRepository.getTheaterByLocation(searchString);
		if(StringUtils.isEmpty(searchString)){
			throw new MTSException(ErrorCode.MISSING_DATA, "parameter not provided");	
		} else {
			theaterList = theaterRepository.getTheaterByLocation(searchString);
		}
		return theaterList;
	}

	@Override
	public Theater addTheater(Theater t) {
		if(StringUtils.isEmpty(t.getName()) || t.getName().length() > MAX_NAME_LENGTH) {			
			throw new InvalidFieldException("Theater name is required");
		}
		if(StringUtils.isEmpty(t.getLocation()) || t.getLocation().length() > MAX_NAME_LENGTH) {			
			throw new InvalidFieldException("Location is required");
		}
		TheaterImpl theater = (TheaterImpl) t;
		int theaterId = theaterRepository.addTheater(theater);
		return getTheaters(theaterId);		
	}

}
