package com.bhavya.mts.service.impl;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.impl.MovieImpl;
import com.bhavya.mts.repository.MovieRepository;
import com.bhavya.mts.service.MovieService;
import com.bhavya.mts.service.exception.ErrorCode;
import com.bhavya.mts.service.exception.InvalidFieldException;
import com.bhavya.mts.service.exception.MTSException;

@Service
public class MovieServiceImpl implements MovieService {
	private static final int MAX_NAME_LENGTH = 50;
	@Autowired
	private MovieRepository movieRepository; 
	
	@Transactional
	public Movie getMovies(int id) {
		return movieRepository.getMovie(id);
	}
	@Override
	@Transactional
	public List<Movie> getMovieNames(String searchString) {
		List<Movie> moviesList = movieRepository.getMovieNames(searchString);
		if(StringUtils.isEmpty(searchString)){
			throw new MTSException(ErrorCode.MISSING_DATA, "parameter not provided");	
		} else {
			moviesList = movieRepository.getMovieNames(searchString);
		}
		return moviesList;
	}
	
	@Override
	@Transactional
	public MovieImpl addMovie(String title, String description, double duration){
		if(StringUtils.isEmpty(title) || title.length() > MAX_NAME_LENGTH) {			
			throw new InvalidFieldException("Movie title is required");
		}
		
		if(StringUtils.isEmpty(description) || description.length() > MAX_NAME_LENGTH) {			
			throw new InvalidFieldException("Description title is required");
		}
		return movieRepository.addMovie(title,description, duration);
	}
	
	@Override
	@Transactional
	public void updateMovie(int movieId,String description, String title){
		movieRepository.updateMovie(movieId,description, title);
	}
	
	@Override
	@Transactional
	public String deleteMovie(int movieId) {
		return movieRepository.deleteMovie(movieId);
		
	}
}
