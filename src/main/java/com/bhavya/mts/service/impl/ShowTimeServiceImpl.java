package com.bhavya.mts.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.repository.ShowTimeRepository;
import com.bhavya.mts.service.ShowTimeService;

@Service
public class ShowTimeServiceImpl implements ShowTimeService {

	@Autowired
	private ShowTimeRepository showTimeRepository; 
	
	@Transactional
	public Movie getMovies(String show_time) {
		return showTimeRepository.getMovie(show_time);
	}

}
