package com.bhavya.mts.service;

import com.bhavya.mts.entity.Movie;

public interface ShowTimeService {

	public Movie getMovies(String show_time);

}
