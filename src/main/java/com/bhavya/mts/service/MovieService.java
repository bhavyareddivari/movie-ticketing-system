package com.bhavya.mts.service;

import java.util.List;

import com.bhavya.mts.entity.Movie;
import com.bhavya.mts.entity.impl.MovieImpl;

public interface MovieService {
		
	void updateMovie(int id,String description, String tittle);
				
	public Movie getMovies(int id);

	List<Movie> getMovieNames(String searchString);

	String deleteMovie(int movieId);

	MovieImpl addMovie(String title, String description, double duration);
		
}
